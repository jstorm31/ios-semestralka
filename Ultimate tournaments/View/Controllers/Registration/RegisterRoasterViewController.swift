//
//  RegisterRoasterViewController.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 10/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import UIKit
import FirebaseDatabase

class RegisterRoasterViewController: BaseViewController {
	
	weak var playersTable: UITableView!
	var viewModel = RoasterViewModel()
	private var registration: Registration
	
	
	init(_ tournament: Tournament) {
		self.registration = Registration(tournament)
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func loadView() {
		super.loadView()
		
		// ----- Navigation item -----
		navigationItem.title = "Výběr soupisky"
		navigationItem.backBarButtonItem = UIBarButtonItem(title: "Soupiska", style: .plain, target: nil, action: nil)
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Pokračovat", style: .done, target: self, action: #selector(continueButtonTapped(_:)))
		navigationItem.rightBarButtonItem!.isEnabled = !viewModel.roaster.isEmpty
		
		// ----- Content view -----
		let contentView = UIView()
		contentView.backgroundColor = .white
		contentView.layer.cornerRadius = 4
		view.addSubview(contentView)
		contentView.snp.makeConstraints { make in
			make.top.bottom.equalTo(view).inset(10)
			make.leading.equalTo(view).offset(10)
			make.trailing.equalTo(view).offset(-10)
		}
		
		// ----- Table view -----
		let players = UITableView()
		players.delegate = self
		players.dataSource = viewModel
		players.register(PlayerTableViewCell.self, forCellReuseIdentifier: "PlayerCell")
		players.tableFooterView = UIView()
		players.allowsMultipleSelection = true
		contentView.addSubview(players)
		players.snp.makeConstraints { make in
			make.size.equalTo(contentView).inset(10)
			make.top.leading.equalTo(contentView).offset(10)
		}
		self.playersTable = players
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		viewModel.fetchPlayers() { [weak self] in
			self?.playersTable.reloadData()
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(true)
		
		// Bugfix: UIBarButtonItem stays highlighted after navigation
		navigationItem.rightBarButtonItem!.isEnabled = false
		navigationItem.rightBarButtonItem!.isEnabled = true
	}
	
	@objc func continueButtonTapped(_ sender: UIBarButtonItem) {
		registration.roaster = viewModel.roaster
		navigationController?.pushViewController(RegisterAccommodationFoodViewController(registration), animated: true)
	}
	
}


//MARK: UITableView delegate
extension RegisterRoasterViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		playersTable.cellForRow(at: indexPath)?.accessoryType = .checkmark
		viewModel.players[indexPath.row].isSelected = true
		
		guard let continueButton = navigationItem.rightBarButtonItem else { return }
		continueButton.isEnabled = !viewModel.roaster.isEmpty
	}
	
	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		playersTable.cellForRow(at: indexPath)?.accessoryType = .none
		viewModel.players[indexPath.row].isSelected = false
		
		guard let continueButton = navigationItem.rightBarButtonItem else { return }
		continueButton.isEnabled = !viewModel.roaster.isEmpty
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 40
	}
	
}
