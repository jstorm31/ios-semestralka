//
//  RegisterOverviewViewController.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 11/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import UIKit

class RegisterOverviewViewController: BaseViewController {
	
	private var registration: Registration
	
	init(_ registration: Registration) {
		self.registration = registration
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func loadView() {
		super.loadView()
		
		let padding = 15
		
		// ----- Scroll view -----
		let scrollView = UIScrollView()
		view.addSubview(scrollView)
		scrollView.snp.makeConstraints { make in
			make.edges.equalTo(view)
			make.top.equalTo(view)
		}
		
		// ----- Content view -----
		let contentView = UIView()
		contentView.backgroundColor = .white
		contentView.layer.cornerRadius = 4
		scrollView.addSubview(contentView)
		contentView.snp.makeConstraints { make in
			make.top.bottom.equalTo(scrollView).inset(10)
			make.leading.equalTo(view).offset(10)
			make.trailing.equalTo(view).offset(-10)
		}
		
		// ----- Title label -----
		let titleLabel = Title2Label(text: "Shrnutí registrace")
		contentView.addSubview(titleLabel)
		titleLabel.snp.makeConstraints { make in
			make.leading.top.equalToSuperview().inset(padding)
		}
	}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
