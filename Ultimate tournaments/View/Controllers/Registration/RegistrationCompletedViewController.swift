//
//  RegistrationCompletedViewController.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 11/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import UIKit

class RegistrationCompletedViewController: BaseViewController {
	
	private let price: Int
	private let bankAccount: String
	
	init(_ price: Int, bankAccount: String) {
		self.price = price
		self.bankAccount = bankAccount
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func loadView() {
		super.loadView()
		
		let padding = 10
		
		// ----- navigation item -----
		navigationItem.title = "Zaregistrováno"
		navigationItem.setHidesBackButton(true, animated: false)
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Hotovo", style: .done, target: self, action: #selector(doneButtonTapped(_:)))
		
		// ----- Content view -----
		let contentView = UIView()
		contentView.backgroundColor = .white
		contentView.layer.cornerRadius = 4
		view.addSubview(contentView)
		contentView.snp.makeConstraints { make in
			make.top.bottom.equalTo(view).inset(20)
			make.leading.equalTo(view).offset(10)
			make.trailing.equalTo(view).offset(-10)
		}
		
		// ----- Title label -----
		let titleLabel = Title2Label(text: "Zaregistrováno!")
		contentView.addSubview(titleLabel)
		titleLabel.snp.makeConstraints { make in
			make.top.equalToSuperview().inset(30)
			make.leading.trailing.equalToSuperview().inset(padding)
		}
		
		// ----- Text -----
		let formattedText =  NSMutableAttributedString()
		formattedText
			.normal("Váš tým byl úspěšně zaregistrován na turnaj.\n\nPošlete prosím částku ")
			.bold("\(price)")
			.normal(" Kč na účet ")
			.bold("\(bankAccount).")
		
		let textLabel = UILabel()
		textLabel.attributedText = formattedText
		textLabel.numberOfLines = 0
		textLabel.lineBreakMode = .byWordWrapping
		contentView.addSubview(textLabel)
		textLabel.snp.makeConstraints { make in
			make.top.equalTo(titleLabel.snp.bottom).offset(20)
			make.leading.trailing.equalToSuperview().inset(padding)
		}
	}
	
	@objc func doneButtonTapped (_ sender: UIBarButtonItem) {
		self.navigationController?.popToRootViewController(animated: true)
	}
	
}
