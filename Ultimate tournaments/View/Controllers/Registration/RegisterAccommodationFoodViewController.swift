//
//  RegisterAccommodationFoodViewController.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 10/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import UIKit

class RegisterAccommodationFoodViewController: BaseViewController {
	
	private var dinnerFields = [NumericTextField]()
	private var accommodationFields = [NumericTextField]()
	private var viewModel: AccommodationViewModel
	
	init(_ registration: Registration) {
		viewModel = AccommodationViewModel(registration)
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func loadView() {
		super.loadView()
		
		let padding = 10
		
		// ----- navigation item -----
		navigationItem.title = "Ubytování / strava"
		navigationItem.backBarButtonItem = UIBarButtonItem(title: "Zpět", style: .plain, target: nil, action: nil)
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Registrovat", style: .done, target: self, action: #selector(continueButtonTapped(_:)))
		
		// ----- Content view -----
		let contentView = UIView()
		contentView.backgroundColor = .white
		contentView.layer.cornerRadius = 4
		view.addSubview(contentView)
		contentView.snp.makeConstraints { make in
			make.top.bottom.equalTo(view).inset(20)
			make.leading.equalTo(view).offset(10)
			make.trailing.equalTo(view).offset(-10)
		}
		
		// ----- Dinners label -----
		let dinners = Title2Label(text: "Večeře")
		contentView.addSubview(dinners)
		dinners.snp.makeConstraints { make in
			make.leading.top.equalToSuperview().inset(padding)
		}
		
		// ----- Dinners container view -----
		let dinnersContainer = UIStackView()
		dinnersContainer.axis = .vertical
		dinnersContainer.spacing = 10
		dinnersContainer.distribution = .fillProportionally
		contentView.addSubview(dinnersContainer)
		dinnersContainer.snp.makeConstraints { make in
			make.top.equalTo(dinners.snp.bottom).offset(15)
			make.leading.trailing.equalToSuperview().inset(padding)
		}
		
		// ------ Dinner items -----
		if let dinners = viewModel.registration.tournament.dinners {
			for dinner in dinners {
				let text = viewModel.getOptionText(for: dinner)
				let field = makeDinnerItem(text: text, containerView: dinnersContainer)
				dinnerFields.append(field)
			}
		}
		
		// ----- Accommodation label -----
		let accommodationLabel = Title2Label(text: "Ubytování")
		contentView.addSubview(accommodationLabel)
		accommodationLabel.snp.makeConstraints { make in
			make.top.equalTo(dinnersContainer.snp.bottom).offset(40)
			make.leading.equalToSuperview().inset(padding)
		}
		
		// ----- Accommodation container view -----
		let accommodationContainer = UIStackView()
		accommodationContainer.axis = .vertical
		accommodationContainer.spacing = 10
		accommodationContainer.distribution = .fillProportionally
		contentView.addSubview(accommodationContainer)
		accommodationContainer.snp.makeConstraints { make in
			make.top.equalTo(accommodationLabel.snp.bottom).offset(15)
			make.leading.trailing.equalToSuperview().inset(padding)
		}
		
		// ------ Accommodation items -----
		if let accommodation = viewModel.registration.tournament.accommodationOptions {
			for option in accommodation {
				let text = viewModel.getOptionText(for: option)
				let field = makeDinnerItem(text: text, containerView: accommodationContainer)
				accommodationFields.append(field)
			}
		}
	}
	
	func makeDinnerItem(text: String, containerView: UIStackView) -> NumericTextField {
		// ----- Dinner item stackView -----
		let dinnerItem = UIStackView()
		dinnerItem.axis = .horizontal
		dinnerItem.spacing = 15
		dinnerItem.distribution = .fillProportionally
		dinnerItem.alignment = .leading
		
		// ---- Dinner count text field -----
		let countTextField = NumericTextField()
		dinnerItem.addArrangedSubview(countTextField)
		countTextField.snp.makeConstraints { make in
			make.width.equalTo(40)
			make.height.equalTo(30)
		}
		
		//----- Dish label -----
		let dishLabel = UILabel()
		dishLabel.text = text
		dishLabel.numberOfLines = 0
		dishLabel.lineBreakMode = .byWordWrapping
		dinnerItem.addArrangedSubview(dishLabel)
		
		containerView.addArrangedSubview(dinnerItem)
		return countTextField
	}
	
	@objc func continueButtonTapped(_ sender: UIBarButtonItem) {
		extractCountsFromFields()
		
		viewModel.submitRegistration(completion: { [weak self] success in
			if success {
				let price = self?.viewModel.registration.getTotalPrice()
				let bankAccount = self?.viewModel.registration.tournament.bankAccount
				self?.navigationController?.pushViewController(RegistrationCompletedViewController(price!, bankAccount: bankAccount!), animated: true)
				
			} else {
				let alert = UIAlertController(title: "Chyba", message: "Při odesílání registrace nastala chyba", preferredStyle: .alert)
	
				alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [weak self] action in
					alert.dismiss(animated: true, completion: nil)
					self?.navigationController?.popToRootViewController(animated: true)
				}))
				
				self?.present(alert, animated: true)
			}
		})
	}
	
	func extractCountsFromFields() {
		viewModel.registration.dinnerCounts = []
		viewModel.registration.accommodationCounts = []
		
		for field in dinnerFields {
			if let text = field.text, let count = Int(text) {
				viewModel.registration.dinnerCounts.append(count)
			} else {
				viewModel.registration.dinnerCounts.append(0)
			}
		}
		
		for field in accommodationFields {
			if let text = field.text, let count = Int(text) {
				viewModel.registration.accommodationCounts.append(count)
			} else {
				viewModel.registration.accommodationCounts.append(0)
			}
		}
	}
	
}
