//
//  TournamentViewController.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 28/12/2017.
//  Copyright © 2017 Jiří Zdvomka. All rights reserved.
//

import UIKit
import SnapKit

class TournamentViewController: BaseViewController {
	
	weak var scrollView: UIScrollView!
	weak var contentView: UIView!
	weak var nameLabel: UILabel!
	weak var locationLabel: UILabel!
	weak var dateStackView: UIStackView!
	weak var fromLabel: UILabel!
	weak var toLabel: UILabel!
	weak var capacityLabel: UILabel!
	weak var teamFeeLabel: UILabel!
	weak var infoLabel: UILabel!
	weak var accommodationLabel: UILabel!
	weak var registerButton: UIButton!
	weak var mapButton: UIButton!
	weak var accommodationOptionsLabel: UILabel!
	weak var foodInfoLabel: UILabel!
	weak var foodOptionsLabel: UILabel!
	weak var playerFeeLabel: UILabel!
	let tournament: Tournament
	
	var viewModel: TournamentsViewModel? = nil {
		didSet {
			guard let viewModel = viewModel else { return }
			
			// Set texts
			fromLabel.text = viewModel.from
			toLabel.text = viewModel.to
			capacityLabel.text = viewModel.capacity
			capacityLabel.textColor = viewModel.capacityColor
			nameLabel.text = viewModel.name
			locationLabel.text = viewModel.location
			teamFeeLabel.text = viewModel.teamFee
			playerFeeLabel.text = viewModel.playerFee
			
			infoLabel.text = viewModel.info
			infoLabel.setLineSpacing(lineSpacing: 1.5)
			
			accommodationLabel.text = viewModel.accommodationInfo
			accommodationLabel.setLineSpacing(lineSpacing: 1.5)
			accommodationOptionsLabel.text = viewModel.accommodationOptions
			
			foodInfoLabel.text = viewModel.foodInfo
			foodInfoLabel.setLineSpacing(lineSpacing: 1.5)
			foodOptionsLabel.text = viewModel.foodOptions
			
			// Register button
			viewModel.observeIsTeamRegistered() { [weak self] in
				self?.registerButton.isEnabled = (self?.viewModel?.registerButtonIsEnabled)!
				self?.registerButton.backgroundColor = self?.registerButton.state == .disabled ? UIColor.MyTheme.lightGreen : UIColor.MyTheme.green
				self?.registerButton.setTitle("Přihlásit", for: .normal)
				self?.registerButton.setTitle(viewModel.registerButtonTitleForDisabled, for: .disabled)
			}
			registerButton.isEnabled = false
		}
	}
	
	
	init(tournament: Tournament) {
		self.tournament = tournament
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func loadView() {
		super.loadView()
		
		let horizontalInset = 15
		
		// ----- Navigation -----
		navigationItem.title = "Detail turnaje"
		navigationItem.backBarButtonItem = UIBarButtonItem(title: "Turnaj", style: .plain, target: nil, action: nil)

		// ----- Scroll view -----
		let scrollView = UIScrollView()
		view.addSubview(scrollView)
		scrollView.snp.makeConstraints { make in
			make.edges.equalTo(view)
			make.top.equalTo(view)
		}
		self.scrollView = scrollView
		
		// ----- Content view -----
		let contentView = UIView()
		contentView.backgroundColor = .white
		contentView.layer.cornerRadius = 4
		scrollView.addSubview(contentView)
		contentView.snp.makeConstraints { make in
			make.top.bottom.equalTo(scrollView).inset(10)
			make.leading.equalTo(view).offset(10)
			make.trailing.equalTo(view).offset(-10)
		}
		self.contentView = contentView
		
		
		// ----- Name label -----
		let name = UILabel()
		name.font = UIFont.boldSystemFont(ofSize: 32)
		contentView.addSubview(name)
		name.snp.makeConstraints { make in
			make.top.equalTo(contentView).offset(20)
			make.leading.trailing.equalTo(contentView).inset(horizontalInset)
		}
		self.nameLabel = name
		
		// ----- Location label -----
		let location = UILabel()
		location.font = UIFont.systemFont(ofSize: 20)
		contentView.addSubview(location)
		location.snp.makeConstraints { make in
			make.leading.trailing.equalTo(contentView).inset(horizontalInset)
			make.top.equalTo(name.snp.bottom).offset(20)
		}
		self.locationLabel = location

		// ----- From / To label -----
		let stack = UIStackView()
		stack.axis = .horizontal
		stack.spacing = 8.0
		contentView.addSubview(stack)
		stack.snp.makeConstraints { make in
			make.leading.equalToSuperview().inset(horizontalInset)
			make.top.equalTo(location.snp.bottom).offset(5)
		}
		self.dateStackView = stack

		let from = UILabel()
		from.textColor = .gray
		stack.addArrangedSubview(from)
		self.fromLabel = from

		let separator = UILabel()
		separator.textColor = .gray
		separator.text = "-"
		stack.addArrangedSubview(separator)

		let to = UILabel()
		to.textColor = .gray
		stack.addArrangedSubview(to)
		self.toLabel = to

		let capacityStack = UIStackView()
		capacityStack.axis = .horizontal
		contentView.addSubview(capacityStack)
		capacityStack.snp.makeConstraints { make in
			make.leading.trailing.equalToSuperview().inset(horizontalInset)
			make.top.equalTo(stack.snp.bottom).offset(15)
		}

		let capacity = UILabel()
		capacityStack.addArrangedSubview(capacity)
		self.capacityLabel = capacity

		let teamFee = UILabel()
		teamFee.textColor = UIColor.MyTheme.blue
		capacityStack.addArrangedSubview(teamFee)
		self.teamFeeLabel = teamFee
		
		// ----- buttons -----
		let buttonsStack = UIStackView()
		buttonsStack.axis = .horizontal
		buttonsStack.spacing = 10
		buttonsStack.distribution = .equalSpacing
		contentView.addSubview(buttonsStack)
		buttonsStack.snp.makeConstraints { make in
			make.top.equalTo(capacityStack.snp.bottom).offset(15)
			make.leading.trailing.equalToSuperview().inset(horizontalInset)
		}
		
		let registerButton = MyButton(title: "Příhlásit", color: UIColor.MyTheme.green)
		registerButton.addTarget(self, action: #selector(registerTeamButtonTapped(_:)), for: .touchUpInside)
		buttonsStack.addArrangedSubview(registerButton)
		self.registerButton = registerButton
		
		let mapButton = MyButton(title: "Mapa", color: UIColor.MyTheme.blue)
		mapButton.addTarget(self, action: #selector(viewMapButtonTapped(_:)), for: .touchUpInside)
		buttonsStack.addArrangedSubview(mapButton)
		self.mapButton = mapButton
		
		// ----- Player fee label ------
		let playerFeeLabel = UILabel()
		contentView.addSubview(playerFeeLabel)
		playerFeeLabel.snp.makeConstraints { make in
			make.top.equalTo(buttonsStack.snp.bottom).offset(20)
			make.leading.trailing.equalToSuperview().inset(horizontalInset)
		}
		self.playerFeeLabel = playerFeeLabel
		
		// ----- Info label -----
		let infoTitle = UILabel()
		infoTitle.font = UIFont.boldSystemFont(ofSize: 20)
		infoTitle.text = "Informace"
		contentView.addSubview(infoTitle)
		infoTitle.snp.makeConstraints { make in
			make.top.equalTo(playerFeeLabel.snp.bottom).offset(30)
			make.leading.trailing.equalToSuperview().inset(horizontalInset)
		}
		
		let info = UILabel()
		info.numberOfLines = 0
		contentView.addSubview(info)
		info.snp.makeConstraints { make in
			make.top.equalTo(infoTitle.snp.bottom).offset(5)
			make.leading.trailing.equalToSuperview().inset(horizontalInset)
		}
		self.infoLabel = info
		
		// ----- Accommodation label -----
		let accommodationTitle = UILabel()
		accommodationTitle.font = UIFont.boldSystemFont(ofSize: 20)
		accommodationTitle.text = "Ubytování"
		contentView.addSubview(accommodationTitle)
		accommodationTitle.snp.makeConstraints { make in
			make.top.equalTo(info.snp.bottom).offset(30)
			make.leading.trailing.equalToSuperview().inset(horizontalInset)
		}
		
		// ------ Accommodation text -----
		let accommodation = UILabel()
		accommodation.numberOfLines = 0
		contentView.addSubview(accommodation)
		accommodation.snp.makeConstraints { make in
			make.top.equalTo(accommodationTitle.snp.bottom).offset(5)
			make.leading.trailing.equalToSuperview().inset(horizontalInset)
		}
		self.accommodationLabel = accommodation
		
		// ----- Accommodation options title -----
		let accommodationOptionsTitle = UILabel()
		accommodationOptionsTitle.font = UIFont.boldSystemFont(ofSize: 16)
		accommodationOptionsTitle.text = "Možnosti: "
		contentView.addSubview(accommodationOptionsTitle)
		accommodationOptionsTitle.snp.makeConstraints { make in
			make.top.equalTo(accommodation.snp.bottom).offset(15)
			make.leading.trailing.equalToSuperview().inset(horizontalInset)
		}
		
		// ----- Accommodation options -----
		let accommodationOptions = UILabel()
		accommodationOptions.numberOfLines = 0
		accommodationOptions.lineBreakMode = .byWordWrapping
		contentView.addSubview(accommodationOptions)
		accommodationOptions.snp.makeConstraints { make in
			make.top.equalTo(accommodationOptionsTitle.snp.bottom).offset(10)
			make.leading.trailing.equalToSuperview().inset(horizontalInset)
		}
		self.accommodationOptionsLabel = accommodationOptions
		
		
		// ------ Food title -----
		let foodTitle = UILabel()
		foodTitle.font = UIFont.boldSystemFont(ofSize: 20)
		foodTitle.text = "Stravování"
		contentView.addSubview(foodTitle)
		foodTitle.snp.makeConstraints { make in
			make.top.equalTo(accommodationOptions.snp.bottom).offset(30)
			make.leading.trailing.equalToSuperview().inset(horizontalInset)
		}
		
		// ------ Food text -----
		let foodInfo = UILabel()
		foodInfo.numberOfLines = 0
		contentView.addSubview(foodInfo)
		foodInfo.snp.makeConstraints { make in
			make.top.equalTo(foodTitle.snp.bottom).offset(5)
			make.leading.trailing.equalToSuperview().inset(horizontalInset)
		}
		self.foodInfoLabel = foodInfo
		
		// ----- Food options title -----
		let foodOptionsTitle = UILabel()
		foodOptionsTitle.font = UIFont.boldSystemFont(ofSize: 16)
		foodOptionsTitle.text = "Možnosti: "
		contentView.addSubview(foodOptionsTitle)
		foodOptionsTitle.snp.makeConstraints { make in
			make.top.equalTo(foodInfo.snp.bottom).offset(15)
			make.leading.trailing.equalToSuperview().inset(horizontalInset)
		}
		
		// ----- Food options -----
		let foodOptions = UILabel()
		foodOptions.numberOfLines = 0
		foodOptions.lineBreakMode = .byWordWrapping
		contentView.addSubview(foodOptions)
		foodOptions.snp.makeConstraints { make in
			make.top.equalTo(foodOptionsTitle.snp.bottom).offset(10)
			make.bottom.equalTo(contentView).offset(-20)
			make.leading.trailing.equalToSuperview().inset(horizontalInset)
		}
		self.foodOptionsLabel = foodOptions
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		viewModel = TournamentsViewModel(model: tournament)
	}
	
	@objc func registerTeamButtonTapped(_ sender: UIButton) {
		navigationController?.pushViewController(RegisterRoasterViewController(tournament), animated: true)
	}
	
	@objc func viewMapButtonTapped(_ sender: UIButton) {
		if let key = tournament.key {
			navigationController?.pushViewController(TournamentMapViewController(tournamentKey: key), animated: true)
		}
	}
	
}
