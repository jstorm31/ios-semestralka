//
//  FinishedTournamentsViewController.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 11/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import UIKit
import FirebaseDatabase

class FinishedTournamentsViewController: BaseViewController {
	
	weak var tournamentsTableView: UITableView!
	weak var fakeView: UIView!
	private var databaseReference: DatabaseReference!
	var tournaments = [Tournament]()
	
	override func loadView() {
		super.loadView()
		
		// ----- Navigation -----
		navigationItem.title = "Skončené turnaje"
		
		let table = UITableView()
		table.delegate = self
		table.dataSource = self
		table.register(FinishedTournamentTableViewCell.self, forCellReuseIdentifier: "FinishedTournamentCell")
		table.backgroundColor = .clear
		view.addSubview(table)
		
		table.snp.makeConstraints { make in
			make.height.equalTo(view)
			make.leading.equalTo(view).offset(10)
			make.trailing.equalTo(view).offset(-10)
		}
		tournamentsTableView = table
		
		let fakeView = UIView()
		fakeView.backgroundColor = UIColor.MyTheme.grayBackground
		view.addSubview(fakeView)
		fakeView.snp.makeConstraints { make in
			make.size.equalToSuperview()
		}
		self.fakeView = fakeView
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		activityIndicator.startAnimating()
		
		// Firebase setup
		databaseReference = Database.database().reference().child("tournaments")
		let today = Date().timeIntervalSince1970
		let query = databaseReference.queryOrdered(byChild: "to").queryEnding(atValue: today)
		
		// Observe child added
		query.observe(.childAdded) { [weak self] snapshot in
			guard let tournamentDict = snapshot.value as? [String: Any] else { return }
			
			if var tournament = Tournament.deserialize(from: tournamentDict) {
				tournament.key = snapshot.key
				self?.tournaments.append(tournament)
				self?.tournamentsTableView.reloadData()
				self?.activityIndicator.stopAnimating()
				self?.fakeView.isHidden = true
			}
		}
		
		// Observe value changed
		query.observe(.childChanged) { [weak self] snapshot in
			guard let tournamentDict = snapshot.value as? [String: Any] else { return }
			
			if var tournament = Tournament.deserialize(from: tournamentDict) {
				tournament.key = snapshot.key
				guard let index = self?.tournaments.index(where: { $0.key == tournament.key }) else { return }
				self?.tournaments[index] = tournament
				self?.tournamentsTableView.reloadData()
			}
		}
		
		// Observe child removed
		query.observe(.childRemoved) { [weak self] snapshot in
			if let index = self?.tournaments.index(where: { $0.key == snapshot.key }) {
				self?.tournaments.remove(at: index)
				self?.tournamentsTableView.reloadData()
			}
		}
	}

}


//MARK: UITableView data source
extension FinishedTournamentsViewController: UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return tournaments.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "FinishedTournamentCell", for: indexPath) as! FinishedTournamentTableViewCell
		cell.tournament = tournaments[indexPath.section]
		
		cell.backgroundColor = .white
		cell.layer.cornerRadius = 4
		cell.selectionStyle = .none
		cell.clipsToBounds = true
		
		return cell
	}
	
	
}

//MARK: UITableView delegate
extension FinishedTournamentsViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		return
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 185
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 10
	}
	
	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 10
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let headerView = UIView()
		headerView.backgroundColor = UIColor.clear
		
		return headerView
	}
	
	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		let footerView = UIView()
		footerView.backgroundColor = UIColor.clear
		
		return footerView
	}
}
