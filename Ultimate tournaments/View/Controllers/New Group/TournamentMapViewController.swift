//
//  TournamentMapViewController.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 01/01/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import UIKit
import SnapKit
import MapKit
import FirebaseDatabase

class TournamentMapViewController: BaseViewController, CLLocationManagerDelegate, MKMapViewDelegate {
	
	weak var mapView: MKMapView!
	weak var placesTable: UITableView!
	var tournamentKey: String
	private var databaseReference: DatabaseReference!
	private var places = [Place]()
	
	init(tournamentKey: String) {
		self.tournamentKey = tournamentKey
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func loadView() {
		super.loadView()
		view.backgroundColor = .white
		
		// ----- MapView -----
		let mapView = MKMapView()
		view.addSubview(mapView)
		mapView.snp.makeConstraints { make in
			make.top.leading.trailing.equalToSuperview()
			make.height.equalTo(300)
		}
		self.mapView = mapView
		
		// ----- Table view -----
		let placesTable = UITableView()
		placesTable.delegate = self
		placesTable.dataSource = self
		placesTable.register(PlaceTableViewCell.self, forCellReuseIdentifier: "PlaceCell")
		placesTable.tableFooterView = UIView()
		placesTable.backgroundColor = .clear
		view.addSubview(placesTable)
		placesTable.snp.makeConstraints { make in
			make.top.equalTo(mapView.snp.bottom).offset(20)
			make.leading.equalToSuperview().offset(10)
			make.trailing.equalToSuperview().offset(-30)
			make.bottom.equalToSuperview()
		}
		self.placesTable = placesTable
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "Důležitá místa"
		
		// Map view
		mapView.delegate = self
		mapView.removeAnnotations(mapView.annotations)

		// Observe Firebase for data
		databaseReference = Database.database().reference().child("places")
		
		fetchData { [weak self] success in
			guard success == true else {
				return
			}
			
			self?.placesTable.reloadData()
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
				self?.zoomMapFitAnnotations()
			})
		}
	}
	
	
	/*
		Will observe .childAdded on firebase database and add new entries, will also add annotation to the MapView
		Has a completion handler in which the TableView should be reloaded
	*/
	func fetchData(completion: @escaping (Bool) -> ()) {
		let observer = databaseReference.queryOrdered(byChild: "tournamentId").queryEqual(toValue: Int(tournamentKey))
		
		observer.observe(.childAdded, with: { [weak self] snapshot in
			guard let placeDict = snapshot.value as? [String : Any] else {
				return
			}
			
			if var place = Place.deserialize(from: placeDict) {
				place.key = snapshot.key
				self?.places.append(place)
				
				// Add annotation to the MapView
				if let annotation = place.toAnnotation() {
					self?.mapView.addAnnotation(annotation)
				}
				
				completion(true)
			}
		}) { error in
			print(error.localizedDescription)
			completion(false)
		}
	}
	
	
	let reuseIdentifier = "annotationReuseIdentifier"
	
	func zoomMapFitAnnotations() {
		var zoomRect = MKMapRectNull
		
		for annotation in mapView.annotations {
			let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
			let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1)
			
			if (MKMapRectIsNull(zoomRect)) {
				zoomRect = pointRect
			} else {
				zoomRect = MKMapRectUnion(zoomRect, pointRect)
			}
		}
		
		self.mapView.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsetsMake(100, 100, 100, 100), animated: true)
	}
}


//MARK: UITableView data source
extension TournamentMapViewController: UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return places.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCell", for: indexPath) as! PlaceTableViewCell
		cell.place = places[indexPath.item]
		
		cell.selectionStyle = .none
		
		return cell
	}
	
}

//MARK: UITableView delegate
extension TournamentMapViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let place = places[indexPath.row]
		
		// Zoom selected place
		if let coordinate = place.coordinate, let title = place.title {
			let annotationPoint = MKMapPointForCoordinate(coordinate)
			let zoomRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 1000, 1000);
			
			// Zoom to annotation area
			mapView.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsetsMake(120, 120, 120, 120), animated: true)
			
			// Select annotation
			if let index = mapView.annotations.index(where: { (annotation: MKAnnotation) -> Bool in
				return (annotation.title as? String) == title
			}) {
				mapView.selectAnnotation(mapView.annotations[index], animated: true)
			}
		}
		
		return
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 80
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 0
	}
	
	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 0
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		return nil
	}
	
	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		return nil
	}
	
}
