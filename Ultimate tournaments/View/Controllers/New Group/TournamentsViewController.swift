//
//  TournamentsViewController.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 28/12/2017.
//  Copyright © 2017 Jiří Zdvomka. All rights reserved.
//

import UIKit
import SnapKit
import FirebaseDatabase

class TournamentsViewController: BaseViewController {
	
	weak var tournamentsTableView: UITableView!
	weak var fakeView: UIView!
	private var databaseReference: DatabaseReference?
	var data = [Tournament]()
	
	override func loadView() {
		super.loadView()
		
		let table = UITableView()
		table.delegate = self
		table.dataSource = self
		table.register(TournamentTableViewCell.self, forCellReuseIdentifier: "TournamentCell")
		table.backgroundColor = .clear
		view.addSubview(table)
		
		table.snp.makeConstraints { make in
			make.height.equalTo(view)
			make.leading.equalTo(view).offset(10)
			make.trailing.equalTo(view).offset(-10)
		}
		tournamentsTableView = table
		
		let fakeView = UIView()
		fakeView.backgroundColor = UIColor.MyTheme.grayBackground
		view.addSubview(fakeView)
		fakeView.snp.makeConstraints { make in
			make.size.equalToSuperview()
		}
		self.fakeView = fakeView
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if let index = self.tournamentsTableView.indexPathForSelectedRow {
			self.tournamentsTableView.deselectRow(at: index, animated: true)
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "Turnaje"
		
		// Show indicator until connected to Firebase
		activityIndicator.startAnimating()
		let connectedRef = Database.database().reference(withPath: ".info/connected")
		connectedRef.observe(.value, with: { [weak self] snapshot in
			if let connected = snapshot.value as? Bool, connected {
				// Connected
				self?.databaseReference = Database.database().reference().child("tournaments")
				self?.observeFirebase() {
					self?.activityIndicator.stopAnimating()
					self?.fakeView.isHidden = true
				}
			}
		})
	}
	
	func observeFirebase(completion: @escaping () -> ()) {
		guard let databaseReference = self.databaseReference else {
			return
		}
		
		// Observe for childAdded
		databaseReference.queryOrdered(byChild: "from").observe(.childAdded, with: { [weak self] snapshot in
			guard let tournamentDict = snapshot.value as? [String: Any] else { return }
			
			if var tournament = Tournament.deserialize(from: tournamentDict) {
				tournament.key = snapshot.key
				self?.data.append(tournament)
				self?.tournamentsTableView.reloadData()
			}
			completion()
			
		}, withCancel: { err in
			print(err.localizedDescription)
			completion()
		})
		
		// Observe value change
		databaseReference.observe(.childChanged, with: { [weak self] snapshot in
			guard let tournamentDict = snapshot.value as? [String: Any] else { return }
			
			if var tournament = Tournament.deserialize(from: tournamentDict) {
				tournament.key = snapshot.key
				guard let index = self?.data.index(where: { $0.key == tournament.key }) else { return }
				self?.data[index] = tournament
				self?.tournamentsTableView.reloadData()
				completion()
			}
		})
		
		// Observe for childRemoved
		databaseReference.observe(.childRemoved, with: { [weak self] snapshot in
			if let index = self?.data.index(where: { $0.key == snapshot.key }) {
				self?.data.remove(at: index)
				self?.tournamentsTableView.reloadData()
			}
			completion()
			
		}, withCancel: { err in
			print(err.localizedDescription)
			completion()
		})
	}
	
}

//MARK: UITableView data source
extension TournamentsViewController: UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return data.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "TournamentCell", for: indexPath) as! TournamentTableViewCell
		cell.tournament = data[indexPath.section]
		
		cell.hideSeparator()
		cell.backgroundColor = .white
		cell.layer.cornerRadius = 4
		cell.clipsToBounds = true
		
		return cell
	}
	
	
}

//MARK: UITableView delegate
extension TournamentsViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let tournament = data[indexPath.section]
		self.navigationController?.pushViewController(TournamentViewController(tournament: tournament), animated: true)
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 140
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 10
	}
	
	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 10
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let headerView = UIView()
		headerView.backgroundColor = UIColor.clear

		return headerView
	}
	
	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		let footerView = UIView()
		footerView.backgroundColor = UIColor.clear
		
		return footerView
	}
}
