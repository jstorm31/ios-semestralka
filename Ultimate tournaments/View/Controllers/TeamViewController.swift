//
//  TeamViewController.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 01/01/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import UIKit
import SnapKit
import FirebaseDatabase

class TeamViewController: BaseViewController {
	
	weak var scrollView: UIScrollView!
	weak var contentView: UIView!
	weak var teamNameTextField: UITextField!
	weak var addPlayerButton: MyButton!
	weak var playersTable: UITableView!
	private var databaseReference: DatabaseReference!
	private var players = [Player]()
	
	
	override func loadView() {
		super.loadView()
		
		let horizontalInset = 15
		
		// ----- Scroll view -----
		let scrollView = UIScrollView()
		view.addSubview(scrollView)
		scrollView.snp.makeConstraints { make in
			make.edges.equalTo(view)
			make.top.equalTo(view)
		}
		self.scrollView = scrollView
		
		// ----- Content stack view -----
		let contentView = UIView()
		contentView.backgroundColor = .white
		contentView.layer.cornerRadius = 4
		scrollView.addSubview(contentView)
		contentView.snp.makeConstraints { make in
			make.top.bottom.equalTo(scrollView).inset(10)
			make.leading.equalTo(view).offset(10)
			make.trailing.equalTo(view).offset(-10)
		}
		self.contentView = contentView
		
		// ----- teamName -----
		let teamNameLabel = Title2Label(text: "Název")
		contentView.addSubview(teamNameLabel)
		teamNameLabel.snp.makeConstraints { make in
			make.top.equalToSuperview().inset(20)
			make.leading.equalToSuperview().inset(horizontalInset)
		}
		
		let teamName = UITextField()
		teamName.borderStyle = .roundedRect
		contentView.addSubview(teamName)
		teamName.snp.makeConstraints { make in
			make.width.equalTo(250)
			make.height.equalTo(35)
			make.leading.equalToSuperview().inset(horizontalInset)
			make.top.equalTo(teamNameLabel.snp.bottom).offset(10)
		}
		self.teamNameTextField = teamName
		
		// ----- Players title -----
		let playersLabel = Title2Label(text: "Hráči")
		contentView.addSubview(playersLabel)
		playersLabel.snp.makeConstraints { make in
			make.top.equalTo(teamName.snp.bottom).offset(30)
			make.leading.equalToSuperview().inset(horizontalInset)
		}
		
		// ----- Add player button -----
		let addPlayerButton = MyButton(title: "Přidat hráče", color: UIColor.MyTheme.green)
		addPlayerButton.addTarget(self, action: #selector(addPlayerButtonTapped(_:)), for: .touchUpInside)
		contentView.addSubview(addPlayerButton)
		addPlayerButton.snp.makeConstraints { make in
			make.top.equalTo(playersLabel.snp.bottom).offset(10)
			make.leading.equalToSuperview().inset(horizontalInset)
		}
		self.addPlayerButton = addPlayerButton
		
		// ---- Players table view -----
		let players = UITableView()
		players.delegate = self
		players.dataSource = self
		players.register(PlayerTableViewCell.self, forCellReuseIdentifier: "PlayerCell")
		players.tableFooterView = UIView()
		players.separatorStyle = .none
		contentView.addSubview(players)
		players.snp.makeConstraints { make in
			make.height.equalTo(305) // Tady by to chtělo nějakou dynamickou šířku, ale jak?
			make.top.equalTo(addPlayerButton.snp.bottom).offset(10)
			make.leading.trailing.equalToSuperview().inset(horizontalInset)
			make.bottom.equalToSuperview().inset(20)
		}
		self.playersTable = players
		
		//init toolbar
		let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
		//create left side empty space so that done button set on right side
		let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
		let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonTapped(_:)))
		toolbar.setItems([flexSpace, doneBtn], animated: false)
		toolbar.sizeToFit()
		//setting toolbar as inputAccessoryView
		self.teamNameTextField.inputAccessoryView = toolbar
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationItem.title = "Nastavení týmu"
		navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveButtonTapped(_:)))
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Upravit", style: .plain, target: self, action: #selector(editButtonTapped(_:)))
				
		databaseReference = Database.database().reference()
		
		// Fetch team name
		if let deviceID = UIDevice.current.identifierForVendor?.uuidString {
			databaseReference.child("teams").child(deviceID).observeSingleEvent(of: .value, with: { [weak self] snapshot in
				let value = snapshot.value as? NSDictionary
				let teamName = value?["name"] as? String ?? ""
				self?.teamNameTextField.text = teamName
			}) { error in
				print (error.localizedDescription)
			}
		}
		
		// Observe players
		observePlayers { [weak self] success in
			guard success == true else {
				return
			}
			self?.playersTable.reloadData()
		}
	}
	
	// Fetching players from Firebase
	func observePlayers(completion: @escaping (Bool) -> ()) {
		guard let deviceID = UIDevice.current.identifierForVendor?.uuidString else {
			return
		}
		let observer = databaseReference.child("players").queryOrdered(byChild: "teamId").queryEqual(toValue: deviceID)
		
		// Observe added
		observer.observe(.childAdded, with: { [weak self] snapshot in
			guard let playerDict = snapshot.value as? [String : Any] else {
				return
			}
			
			if let player = Player.deserialize(from: playerDict, key: snapshot.key) {
				self?.players.append(player)
				completion(true)
			}
		}) { error in
			print(error.localizedDescription)
			completion(false)
		}
		
		// Observe removed
		observer.observe(.childRemoved, with: { [weak self] snapshot in
			let index = self?.players.index(where: { $0.key == snapshot.key })
			if let index = index {
				self?.players.remove(at: index)
			}
			completion(true)
		})
	}
	
	@objc func saveButtonTapped(_ sender: UIBarButtonItem) {
		let teamName = teamNameTextField.text
		
		// Set team name
		if let ref = databaseReference {
			guard let deviceID = UIDevice.current.identifierForVendor?.uuidString else {
				return
			}
			
			ref.child("teams").child(deviceID).setValue(["name": teamName])
			teamNameTextField.endEditing(true)
		}
	}
	
	@objc func doneButtonTapped(_ sender: UIBarButtonItem) {
		self.view.endEditing(true)
	}
	
	@objc func editButtonTapped(_ sender: UIBarButtonItem) {
		playersTable.setEditing(!playersTable.isEditing, animated: true)
		navigationItem.rightBarButtonItem?.title = !playersTable.isEditing ? "Upravit" : "Hotovo"
		navigationItem.rightBarButtonItem?.style = !playersTable.isEditing ? .plain : .done
	}
	
	@objc func addPlayerButtonTapped(_ sender: MyButton) {
		// Alert
		let alert = UIAlertController(title: "Přidat hráče", message: "Zadejte jméno a číslo hráče", preferredStyle: .alert)
		alert.addTextField(configurationHandler: { textfield in })
		
		// Cancel action
		alert.addAction(UIAlertAction(title: "Storno", style: .cancel, handler: { action in
			alert.dismiss(animated: true, completion: nil)
		}))
		
		// Submit action
		alert.addAction(UIAlertAction(title: "Přidat", style: .default, handler: { [weak self] action in
			let text = alert.textFields?.first?.text
			
			guard let player = TeamViewModel.parsePlayer(fromText: text!) else {
				print("Nelze přidat hráče (špatný formát)")
				alert.dismiss(animated: true, completion: nil)
				return
			}
			
			// Add player to Firebase and table
			self?.addPlayer(player)
		}))
		
		self.present(alert, animated: true)
	}
	
	func addPlayer(_ player: Player) {
		let newPlayer = databaseReference.child("players").childByAutoId()
		
		guard let deviceID = UIDevice.current.identifierForVendor?.uuidString else { return }
		var value = player.serialize()
		value["teamId"] = deviceID
		
		newPlayer.setValue(value)
	}
	
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		teamNameTextField.resignFirstResponder()
		return true
	}
}


//MARK: UITableView data source
extension TeamViewController: UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return players.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCell", for: indexPath) as! PlayerTableViewCell
		cell.player = players[indexPath.item]
		
		return cell
	}
}

//MARK: UITableView delegate
extension TeamViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		return
	}
	
	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			if let playerKey = players[indexPath.row].key {
				databaseReference.child("players").child(playerKey).removeValue()
			}
		}
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 40
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 0
	}
	
	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 0
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		return nil
	}
	
	func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		return nil
	}
}
