//
//  ViewController.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 28/12/2017.
//  Copyright © 2017 Jiří Zdvomka. All rights reserved.
//

import UIKit
import SnapKit

class BaseViewController: UIViewController {
	
	weak var activityIndicator: UIActivityIndicatorView!
	weak var noConnectionLabel: UILabel!
	
	override func loadView() {
		super.loadView()
		
		navigationController?.navigationBar.backgroundColor = UIColor.MyTheme.grayBackground
		view.backgroundColor = UIColor.MyTheme.grayBackground
		
		self.edgesForExtendedLayout = [] // Do not show content under navigation bar
		
		// ----- Activity indicator -----
		let window = UIApplication.shared.keyWindow!
		let loader = UIActivityIndicatorView()
		loader.activityIndicatorViewStyle = .gray
		loader.hidesWhenStopped = true
		window.addSubview(loader)
		loader.snp.makeConstraints { make in
			make.edges.equalToSuperview()
			make.center.equalToSuperview()
		}
		self.activityIndicator = loader
		
		// ----- noConnecitonLabel -----
		let noConnection = UILabel()
		noConnection.text = "Cannot establish connection to database"
		noConnection.textAlignment = .center
		noConnection.textColor = UIColor.MyTheme.red
		view.addSubview(noConnection)
		noConnection.snp.makeConstraints { make in
			make.centerX.equalToSuperview().inset(10)
			make.top.equalToSuperview().inset(120)
		}
		self.noConnectionLabel = noConnection
		self.noConnectionLabel.isHidden = true
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
}

