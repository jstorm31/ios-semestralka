//
//  TournamentTableViewCell.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 28/12/2017.
//  Copyright © 2017 Jiří Zdvomka. All rights reserved.
//

import UIKit
import SnapKit

class FinishedTournamentTableViewCell: UITableViewCell {
	
	weak var nameLabel: UILabel!
	weak var locationLabel: UILabel!
	weak var dateStackView: UIStackView!
	weak var fromLabel: UILabel!
	weak var toLabel: UILabel!
	weak var ratingControl: UISegmentedControl!
	weak var ratedLabel: UILabel!
	
	var tournament: Tournament? = nil {
		didSet {
			nameLabel.text = tournament?.name
			locationLabel.text = tournament?.location
			viewModel = FinishedTournamentsCellViewModel(model: tournament!)
		}
	}
	
	var viewModel: FinishedTournamentsCellViewModel? = nil {
		didSet {
			guard let viewModel = viewModel else { return }
			
			fromLabel.text = viewModel.from
			toLabel.text = viewModel.to
			ratingControl.isHidden = viewModel.teamRated
			ratedLabel.isHidden = !viewModel.teamRated
			ratedLabel.text = viewModel.rating
		}
	}
	
	override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		
		// ----- Name label -----
		let name = UILabel()
		name.font = UIFont.boldSystemFont(ofSize: 24)
		contentView.addSubview(name)
		name.snp.makeConstraints { make in
			make.leading.trailing.top.equalToSuperview().inset(10)
		}
		self.nameLabel = name
		
		// ----- Location label -----
		let location = UILabel()
		contentView.addSubview(location)
		location.snp.makeConstraints { make in
			make.leading.trailing.equalToSuperview().inset(10)
			make.top.equalTo(name.snp.bottom).offset(10)
		}
		self.locationLabel = location
		
		// ----- From / To label -----
		let stack = UIStackView()
		stack.axis = .horizontal
		stack.spacing = 8.0
		contentView.addSubview(stack)
		stack.snp.makeConstraints { make in
			make.leading.equalToSuperview().inset(10)
			make.top.equalTo(location.snp.bottom).offset(5)
		}
		self.dateStackView = stack
		
		let from = UILabel()
		from.textColor = .gray
		stack.addArrangedSubview(from)
		self.fromLabel = from
		
		let separator = UILabel()
		separator.textColor = .gray
		separator.text = "-"
		stack.addArrangedSubview(separator)
		
		let to = UILabel()
		to.textColor = .gray
		stack.addArrangedSubview(to)
		self.toLabel = to
		
		// ------ Rating label ------
		let ratingLabel = UILabel()
		ratingLabel.font = UIFont.boldSystemFont(ofSize: 18)
		ratingLabel.text = "Hodnocení:"
		contentView.addSubview(ratingLabel)
		ratingLabel.snp.makeConstraints { make in
			make.top.equalTo(stack.snp.bottom).offset(15)
			make.leading.trailing.equalToSuperview().inset(10)
		}
		
		// ------ Rating component -----
		let ratingControl = UISegmentedControl()
		for i in 1...5 {
			ratingControl.insertSegment(withTitle: String(i), at: i, animated: true)
		}
		ratingControl.addTarget(self, action: #selector(submitRating(_:)), for: .valueChanged)
		contentView.addSubview(ratingControl)
		ratingControl.snp.makeConstraints { make in
			make.top.equalTo(ratingLabel.snp.bottom).offset(5)
			make.bottom.equalToSuperview().inset(10)
			make.leading.trailing.equalToSuperview().inset(10)
		}
		self.ratingControl = ratingControl
		
		// ----- Rated label ------
		let ratedLabel = UILabel()
		ratedLabel.font = UIFont.systemFont(ofSize: 24)
		contentView.addSubview(ratedLabel)
		ratedLabel.snp.makeConstraints { make in
			make.top.equalTo(ratingLabel.snp.bottom).offset(5)
			make.leading.trailing.equalToSuperview().inset(10)
		}
		self.ratedLabel = ratedLabel
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	@objc func submitRating(_ sender: UISegmentedControl) {
		guard let viewModel = viewModel else { return }
		let rating = sender.selectedSegmentIndex + 1
		
		viewModel.submitRating(rating)
	}
}

