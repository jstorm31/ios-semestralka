//
//  TournamentTableViewCell.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 28/12/2017.
//  Copyright © 2017 Jiří Zdvomka. All rights reserved.
//

import UIKit
import SnapKit

class TournamentTableViewCell: UITableViewCell {
	
	weak var nameLabel: UILabel!
	weak var locationLabel: UILabel!
	weak var dateStackView: UIStackView!
	weak var fromLabel: UILabel!
	weak var toLabel: UILabel!
	weak var capacityLabel: UILabel!
	weak var teamFeeLabel: UILabel!
	
	var tournament: Tournament? = nil {
		didSet {
			nameLabel.text = tournament?.name
			locationLabel.text = tournament?.location
			if let teamFee = tournament?.teamFee {
				teamFeeLabel.text = "\(teamFee) Kč"
			}
			viewModel = TournamentsViewModel(model: tournament!)
		}
	}
	
	var viewModel: TournamentsViewModel? = nil {
		didSet {
			fromLabel.text = viewModel?.from
			toLabel.text = viewModel?.to
			capacityLabel.text = viewModel?.capacity
			capacityLabel.textColor = viewModel?.capacityColor
		}
	}
	
	override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		
		// ----- Name label -----
		let name = UILabel()
		name.font = UIFont.boldSystemFont(ofSize: 24)
		contentView.addSubview(name)
		name.snp.makeConstraints { make in
			make.leading.trailing.top.equalToSuperview().inset(10)
		}
		self.nameLabel = name
		
		// ----- Location label -----
		let location = UILabel()
		contentView.addSubview(location)
		location.snp.makeConstraints { make in
			make.leading.trailing.equalToSuperview().inset(10)
			make.top.equalTo(name.snp.bottom).offset(10)
		}
		self.locationLabel = location
		
		// ----- From / To label -----
		let stack = UIStackView()
		stack.axis = .horizontal
		stack.spacing = 8.0
		contentView.addSubview(stack)
		stack.snp.makeConstraints { make in
			make.leading.equalToSuperview().inset(10)
			make.top.equalTo(location.snp.bottom).offset(5)
		}
		self.dateStackView = stack
		
		let from = UILabel()
		from.textColor = .gray
		stack.addArrangedSubview(from)
		self.fromLabel = from
		
		let separator = UILabel()
		separator.textColor = .gray
		separator.text = "-"
		stack.addArrangedSubview(separator)
		
		let to = UILabel()
		to.textColor = .gray
		stack.addArrangedSubview(to)
		self.toLabel = to
		
		let capacityStack = UIStackView()
		capacityStack.axis = .horizontal
		contentView.addSubview(capacityStack)
		capacityStack.snp.makeConstraints { make in
			make.leading.trailing.equalToSuperview().inset(10)
			make.top.equalTo(stack.snp.bottom).offset(15)
		}
		
		let capacity = UILabel()
		capacityStack.addArrangedSubview(capacity)
		self.capacityLabel = capacity
		
		let teamFee = UILabel()
		teamFee.textColor = UIColor.MyTheme.blue
		capacityStack.addArrangedSubview(teamFee)
		self.teamFeeLabel = teamFee
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
