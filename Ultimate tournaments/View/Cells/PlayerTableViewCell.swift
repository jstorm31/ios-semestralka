//
//  PlayerTableViewCell.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 01/01/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import UIKit
import SnapKit

class PlayerTableViewCell: UITableViewCell {
	
	weak var nameLabel: UILabel!
	weak var numberLabel: UILabel!
	
	var player: Player? = nil {
		didSet {
			if let player = player {
				nameLabel.text = player.name
				numberLabel.text = "\(player.number)"
			}
		}
	}
	
	override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		
		self.separatorInset = .zero
		self.layoutMargins = .zero
		
		// ----- stack view -----
		let stack = UIStackView()
		stack.axis = .horizontal
		stack.spacing = 5
		contentView.addSubview(stack)
		stack.snp.makeConstraints { make in
			make.edges.equalToSuperview()
		}
		
		// ----- name label -----
		let name = UILabel()
		stack.addArrangedSubview(name)
		self.nameLabel = name
		
		// ----- name label -----
		let number = UILabel()
		number.textColor = .gray
		stack.addArrangedSubview(number)
		self.numberLabel = number
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
}
