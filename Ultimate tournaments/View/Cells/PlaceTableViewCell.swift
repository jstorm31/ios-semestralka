//
//  PlaceTableViewCell.swift
//  Ultimate tourtitlents
//
//  Created by Jiří Zdvomka on 02/01/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import UIKit

class PlaceTableViewCell: UITableViewCell {

	weak var titleLabel: UILabel!
	weak var addressLabel: UILabel!
	
	var place: Place? = nil {
		didSet {
			if let place = place {
				titleLabel.text = place.title
				if let address = place.address {
					addressLabel.text = "\(address)"
				}
			}
		}
	}
	
	override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		
		// ----- title label -----
		let title = UILabel()
		contentView.addSubview(title)
		title.snp.makeConstraints { make in
			make.leading.top.trailing.equalToSuperview().inset(10)
		}
		self.titleLabel = title
		
		// ----- copy address button -----
		let copy = UIButton()
		copy.setImage(UIImage(named: "icon-copy.png"), for: .normal)
		copy.backgroundColor = UIColor.MyTheme.green
		copy.tintColor = .white
		copy.contentEdgeInsets = UIEdgeInsets(top: 5, left: 8, bottom: 5, right: 8)
		copy.layer.cornerRadius = 4
		copy.addTarget(self, action: #selector(copyButtonTapped(_:)), for: .touchUpInside)
		contentView.addSubview(copy)
		copy.snp.makeConstraints { make in
			make.centerY.equalToSuperview()
			make.trailing.equalToSuperview().inset(10)
		}
		
		// ----- address label -----
		let address = UILabel()
		address.textColor = .gray
		address.lineBreakMode = .byWordWrapping
		address.numberOfLines = 2
		contentView.addSubview(address)
		address.snp.makeConstraints { make in
			make.leading.equalToSuperview().inset(10)
			make.trailing.equalTo(copy.snp.leading).offset(-10)
			make.top.equalTo(title.snp.bottom)
		}
		self.addressLabel = address
	}
	
	@objc func copyButtonTapped(_ sender: UIButton) {
		guard let place = place else {
			return
		}
		
		if let address = place.address {
			UIPasteboard.general.string = address
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

}
