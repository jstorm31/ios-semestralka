//
//  NumericTextField.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 11/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import UIKit
import Foundation

class NumericTextField: UITextField {
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		borderStyle = .roundedRect
		keyboardType = .numberPad
		text = "0"
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
