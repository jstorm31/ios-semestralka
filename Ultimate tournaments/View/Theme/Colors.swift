//
//  Colors.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 28/12/2017.
//  Copyright © 2017 Jiří Zdvomka. All rights reserved.
//

import Foundation
import UIKit
import SwiftHEXColors

extension UIColor {
	struct MyTheme {
		static var grayBackground: UIColor { return UIColor(hexString: "#ECF0F3")! }
		static var grayBorder: UIColor { return UIColor(hexString: "#ACBBCA")! }
		static var blue: UIColor { return UIColor(hexString: "#6D98BA")! }
		static var vanilla: UIColor { return UIColor(hexString: "#D3B99F")! }
		static var red: UIColor { return UIColor(hexString: "#C17767")! }
		static var green: UIColor { return UIColor(hexString: "#5cb85c")! }
		static var lightGreen: UIColor { return UIColor(hexString: "#9DD49D")! }
	}
}
