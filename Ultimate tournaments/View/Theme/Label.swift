//
//  Label.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 30/12/2017.
//  Copyright © 2017 Jiří Zdvomka. All rights reserved.
//

import Foundation
import UIKit

class Title2Label: UILabel {
	
	init(text: String, frame: CGRect = CGRect()) {
		super.init(frame: frame)

		self.font = UIFont.boldSystemFont(ofSize: 22)
		self.text = text
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

extension UILabel {
	
	func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {
		
		guard let labelText = self.text else { return }
		
		let paragraphStyle = NSMutableParagraphStyle()
		paragraphStyle.lineSpacing = lineSpacing
		paragraphStyle.lineHeightMultiple = lineHeightMultiple
		
		let attributedString:NSMutableAttributedString
		if let labelattributedText = self.attributedText {
			attributedString = NSMutableAttributedString(attributedString: labelattributedText)
		} else {
			attributedString = NSMutableAttributedString(string: labelText)
		}
		
		// Line spacing attribute
		attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
		
		self.attributedText = attributedString
	}
}
