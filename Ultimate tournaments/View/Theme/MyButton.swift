//
//  MyButton.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 01/01/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import Foundation
import UIKit

class MyButton: UIButton {
	
	init(title: String, color: UIColor, frame: CGRect = CGRect()) {
		super.init(frame: frame)
		
		setTitle(title, for: .normal)
		titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
		backgroundColor = color
		tintColor = .white
		contentEdgeInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
		layer.cornerRadius = 4
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setBackgroundColor(color: UIColor, forState: UIControlState) {
		UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
		UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
		UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
		let colorImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		self.setBackgroundImage(colorImage, for: forState)
	}
	
}
