//
//  MyRoundedButton.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 24/01/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import Foundation
import UIKit

class MyRoundedButton: UIButton {
	
	init(title: String, color: UIColor, frame: CGRect = CGRect()) {
		super.init(frame: frame)
		
		self.setTitle(title, for: .normal)
		self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
		self.backgroundColor = color
		self.tintColor = .white
		self.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
		self.layer.cornerRadius = 4
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
}
