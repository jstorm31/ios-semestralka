//
//  UITableViewCellExtension.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 12/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell {
	
	func hideSeparator() {
		self.separatorInset = UIEdgeInsets(top: 0, left: self.bounds.size.width, bottom: 0, right: 0)
	}
	
	func showSeparator() {
		self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
	}
}
