//
//  StringExtension.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 11/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import Foundation
import UIKit

extension NSMutableAttributedString {
	@discardableResult func bold(_ text: String) -> NSMutableAttributedString {
		let attrs: [NSAttributedStringKey: Any] = [.font: UIFont.boldSystemFont(ofSize: 16)]
		let boldString = NSMutableAttributedString(string:text, attributes: attrs)
		append(boldString)
		
		return self
	}
	
	@discardableResult func normal(_ text: String) -> NSMutableAttributedString {
		let normal = NSAttributedString(string: text)
		append(normal)
		
		return self
	}
}
