//
//  File.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 01/01/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import Foundation

struct Player {
	var key: String?
	var name: String
	var number: Int
	
	init(_ key: String, _ name: String, _ number: Int) {
		self.key = key
		self.name = name
		self.number = number
	}
	
	init(_ name: String, _ number: Int) {
		self.name = name
		self.number = number
	}
	
	static func deserialize(from dict: [String: Any], key: String) -> Player? {
		if let name = dict["name"] as? String,let number = dict["number"] as? Int {
			return Player(key, name, number)
		}
		return nil
	}
	
	static func deserializeArray(from dict: [String: Any]) -> [Player] {
		var players = [Player]()
		
		for (key, value) in dict {
			if let player = deserialize(from: (value as? [String: Any])!, key: key) {
				players.append(player)
			}
		}
		
		return players
	}
	
	func serialize() -> [String: Any] {
		return [
			"name": name,
			"number": number
		]
	}
}
