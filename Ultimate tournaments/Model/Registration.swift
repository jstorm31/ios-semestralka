//
//  Registration.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 10/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import Foundation

struct Registration {
	let tournament: Tournament
	var roaster: [String]?
	var dinnerCounts: [Int] = []
	var accommodationCounts: [Int] = []
	
	init (_ tournament: Tournament) {
		self.tournament = tournament
	}
	
	func getTotalPrice() -> Int {
		guard let roaster = roaster else { return -1 }
		guard let playerFee = tournament.playerFee, let teamFee = tournament.teamFee else { return -2 }
		
		var dinnersPrice = 0
		var accommodationPrice = 0
		let playerFees = roaster.count * playerFee
		
		for (index, option) in dinnerCounts.enumerated() {
			dinnersPrice += tournament.dinners![index].price * option
		}
		
		for (index, option) in accommodationCounts.enumerated() {
			accommodationPrice += tournament.accommodationOptions![index].price * option
		}
		
		return teamFee + playerFees + dinnersPrice + accommodationPrice
	}
}
