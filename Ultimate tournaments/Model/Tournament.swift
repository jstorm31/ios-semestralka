//
//  Tournament.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 28/12/2017.
//  Copyright © 2017 Jiří Zdvomka. All rights reserved.
//

import Foundation
import UIKit

struct Tournament {
	var key: String?
	var name: String?
	var location: String?
	var from: Date?
	var to: Date?
	var info: String?
	var accommodation: String?
	var capacity: Int?
	var teamFee: Int?
	var playerFee: Int?
	var foodInfo: String?
	var dinners: [Option]?
	var accommodationOptions: [Option]?
	var bankAccount: String?
	var registeredTeams: [String]?
	var rating: Double?
	var teamRated = false
	
	init() {}
	
	init(key: String, name: String, location: String, from: Date, to: Date) {
		self.name = name
		self.location = location
		self.from = from
		self.to = to
	}
	
	static func deserialize(from dict: [String: Any]) -> Tournament? {
		var result = Tournament()
		
		if let name = dict["name"] as? String {
			result.name = name
		} else {
			return nil
		}
		
		if let location = dict["location"] as? String {
			result.location = location
		} else {
			return nil
		}
		
		if let from = dict["from"] as? Double {
			result.from = NSDate(timeIntervalSince1970: from) as Date
		} else {
			return nil
		}
		
		if let to = dict["to"] as? Double {
			result.to = NSDate(timeIntervalSince1970: to) as Date
		} else {
			return nil
		}
		
		if let info = dict["info"] as? String {
			result.info = info
		}
		
		if let accommodation = dict["accommodation"] as? String {
			result.accommodation = accommodation
		}
		
		if let capacity = dict["capacity"] as? Int {
			result.capacity = capacity
		}
		
		if let teamFee = dict["teamFee"] as? Int {
			result.teamFee = teamFee
		}
		
		if let playerFee = dict["playerFee"] as? Int {
			result.playerFee = playerFee
		}
		
		if let foodInfo = dict["foodInfo"] as? String {
			result.foodInfo = foodInfo
		}
		
		if let dinners = dict["dinner"] as? [[String: Any]] {
			var options: [Option] = []

			for value in dinners {
				if let title = value["title"] as? String, let price = value["price"] as? Int {
					options.append(Option(title: title, price: price))
				}
			}

			result.dinners = options
		}
		
		if let accommodationOptions = dict["accommodationOptions"] as? [[String: Any]] {
			var options: [Option] = []
			
			for value in accommodationOptions {
				if let title = value["title"] as? String, let price = value["price"] as? Int {
					options.append(Option(title: title, price: price))
				}
			}
			
			result.accommodationOptions = options
		}
		
		if let bankAccount = dict["bankAccount"] as? String {
			result.bankAccount = bankAccount
		}
		
		if let registeredTeams = dict["registeredTeams"] as? [String: String] {
			result.registeredTeams = registeredTeams.map { "\($1)" }
		}
		
		// Tournament rating
		if let ratings = dict["ratings"] as? [String: [String: Any]] {
			var sumRating = 0
			
			// Total rating + if team has rated
			for value in ratings.values {
				if let rating = (value["rating"] as? Int) {
					sumRating = sumRating + rating
				}
				
				if let recievedTeamId = (value["teamId"] as? String), let teamId = UIDevice.current.identifierForVendor?.uuidString {
					if teamId == recievedTeamId {
						result.teamRated = true
					}
				}
			}
			
			result.rating = Double(sumRating / ratings.count)
		}
		
		return result
	}
	
	func serialize() -> [String: Any] {
		let dict: [String: Any] = [
			"name": self.name!,
			"location": self.location!,
			"from": self.from!.timeIntervalSince1970,
			"to": self.to!.timeIntervalSince1970
		]
		
		return dict
	}
	
	func capacityFull() -> Bool? {
		if let capacity = self.capacity {
			let occupied = registeredTeams?.count ?? 0
			return capacity == occupied
		} else {
			return nil
		}
	}
}
