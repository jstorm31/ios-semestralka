//
//  Place.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 02/01/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

struct Place {
	var key: String?
	var title: String?
	var address: String?
	var coordinate: CLLocationCoordinate2D?
	
	static func deserialize(from dict: [String: Any]) -> Place? {
		var place = Place()
		
		// Title
		if let title = dict["title"] as? String {
			place.title = title
		} else {
			return nil
		}
		
		// Location
		if let address = dict["address"] as? String {
			place.address = address
		} else {
			return nil
		}
		
		// Coordinate
		if let lat = dict["lat"] as? CLLocationDegrees, let lon = dict["lon"] as? CLLocationDegrees {
			place.coordinate = CLLocationCoordinate2DMake(lat, lon)
		} else {
			return nil
		}
		
		return place
	}
	
	func toAnnotation() -> MKPointAnnotation? {
		let annotation = MKPointAnnotation()
		
		if let coordinate = self.coordinate {
			annotation.coordinate = coordinate
		} else {
			return nil
		}
		
		if let title = self.title {
			annotation.title = title
		} else {
			return nil
		}
		
		return annotation
	}
}
