//
//  Option.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 11/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import Foundation

struct Option {
	var title: String
	var price: Int
	
	init(title: String, price: Int) {
		self.title = title
		self.price = price
	}
}
