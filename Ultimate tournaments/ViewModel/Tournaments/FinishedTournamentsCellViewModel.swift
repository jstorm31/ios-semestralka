//
//  FinishedTournamentsViewModel.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 11/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import Foundation
import UIKit

class FinishedTournamentsCellViewModel: TournamentsViewModel {
	var teamRated: Bool {
		get {
			return model.teamRated
		}
	}
	
	var rating: String {
		get {
			guard let rating = model.rating else { return "" }
			var stringRating = ""
			
			let roundedRating = Int(round(1000 * rating) / 1000)
			for _ in 1...roundedRating {
				stringRating += "⭐️"
			}
			
			return stringRating
		}
	}
	
	func submitRating(_ rating: Int) {
		guard let teamID = UIDevice.current.identifierForVendor?.uuidString else { return }
		guard let tournamentKey = model.key else { return }
		let newRef = databaseReference.child("tournaments").child(tournamentKey).child("ratings").childByAutoId()
		
		let value: [String: Any] = [
			"rating": rating,
			"teamId": teamID
		]
		
		newRef.setValue(value, andPriority: 1, withCompletionBlock: { error, ref in
			if let error = error {
				print("Error submiting rating: \(error.localizedDescription)")
			}
		})
	}
}
