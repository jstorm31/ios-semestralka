//
//  TournamentsViewModel.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 28/12/2017.
//  Copyright © 2017 Jiří Zdvomka. All rights reserved.
//

import Foundation
import FirebaseDatabase
import UIKit

class TournamentsViewModel {
	var model: Tournament
	let databaseReference: DatabaseReference
	let bulletPoint = "\u{2022}"
	
	var from: String {
		get {
			return (model.from != nil) ? formatDate(model.from!) : ""
		}
	}
	
	var to: String {
		get {
			return (model.to != nil) ? formatDate(model.to!) : ""
		}
	}
	
	var name: String {
		get {
			return model.name ?? ""
		}
	}
	
	var location: String {
		get {
			return model.location ?? ""
		}
	}
	
	var teamFee: String {
		if let teamFee = model.teamFee {
			return "\(teamFee) Kč"
		}
		return ""
	}
	
	var occupied: Int {
		get {
			return model.registeredTeams?.count ?? 0
		}
	}
	
	var capacity: String {
		get {
			if let capacity = model.capacity {
				return "\(occupied) / \(capacity) týmů"
			}
			return ""
		}
	}
	
	var capacityColor: UIColor {
		get {
			if let full = model.capacityFull() {
				if full {
					return UIColor.MyTheme.red
				} else {
					return UIColor.MyTheme.green
				}
			}
			return UIColor.black
		}
	}
	
	var isCapacityFull: Bool {
		get {
			if let capacity = model.capacity {
				return occupied == capacity
			}
			return false
		}
	}
	var isTeamRegistered: Bool?
	
	var registerButtonIsEnabled: Bool {
		get {
			return !isCapacityFull && !(isTeamRegistered != nil ?? false)
		}
	}
	
	var registerButtonTitleForDisabled: String {
		get {
			var text = "Přihlásit"
			
			if isCapacityFull {
				text = "Plná kapacita"
			} else if let isTeamRegistered = isTeamRegistered, isTeamRegistered == true {
				text = "Přihlášeno"
			}
			
			return text
		}
	}
	
	var info: String {
		get {
			return model.info ?? ""
		}
	}
	
	var accommodationInfo: String {
		get {
			return model.accommodation ?? ""
		}
	}
	
	var accommodationOptions: String {
		get {
			var optionsText = ""
			
			if let options = model.accommodationOptions {
				for option in options {
					let formattedText = "\(bulletPoint)   \(option.title) (\(option.price) Kč)\n"
					optionsText = optionsText + formattedText
				}
			}
			
			return optionsText
		}
	}
	
	var foodInfo: String {
		get {
			return model.foodInfo ?? ""
		}
	}
	
	var foodOptions: String {
		get {
			var optionsText = ""
			
			if let options = model.dinners {
				for option in options {
					let formattedText = "\(bulletPoint)   \(option.title) (\(option.price) Kč)\n"
					optionsText = optionsText + formattedText
				}
			}
			
			return optionsText
		}
	}

	var playerFee: String {
		get {
			return (model.playerFee != nil) ? "Player fee: \(model.playerFee!) Kč" : ""
		}
	}
	
	init (model: Tournament) {
		self.model = model
		databaseReference = Database.database().reference()
	}
	
	// Format date
	func formatDate(_ date: Date) -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.timeStyle = .none
		dateFormatter.dateStyle = .medium
		dateFormatter.timeZone = TimeZone.current
		
		return dateFormatter.string(from: date)
	}
	
	func observeIsTeamRegistered(completion: @escaping () -> ()) {
		guard let teamID = UIDevice.current.identifierForVendor?.uuidString else { return }
		guard let tournamentKey = model.key else { return }
		let ref = databaseReference.child("tournaments").child(tournamentKey).child("registeredTeams").queryOrderedByValue().queryEqual(toValue: teamID)

		ref.observe(.value, with: { [weak self] snapshot in
			self?.isTeamRegistered = (snapshot.value as? [String: String]) != nil
			completion()
		})
	}
}
