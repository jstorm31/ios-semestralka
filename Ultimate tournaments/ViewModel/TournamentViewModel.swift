//
//  TournamentViewModel.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 11/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import Foundation

class TournamentViewModel {
	private let model: Tournament
	
	init(model: Tournament) {
		self.model = model
	}
}
