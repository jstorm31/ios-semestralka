//
//  RoasterViewModel.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 10/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import UIKit
import Foundation
import FirebaseDatabase

class RoasterViewModel: NSObject {
	
	let databaseReference = Database.database().reference().child("players")
	var players = [RoasterViewModelItem]()
	
	var roaster: [String] {
		var result = [String]()
		
		for item in players {
			if item.isSelected {
				result.append(item.player.key!)
			}
		}
		
		return result
   }
	
	
	func fetchPlayers(completion: @escaping () -> ()) {
		observePlayers { [weak self] players in
			guard let players = players else { return }
			self?.players = players.map { RoasterViewModelItem(player: $0) }
			completion()
		}
	}
	
	private func observePlayers(completion: @escaping ([Player]?) -> ()) {
		guard let deviceID = UIDevice.current.identifierForVendor?.uuidString else { return }
		let query = databaseReference.queryOrdered(byChild: "teamId").queryEqual(toValue: deviceID)
		
		query.observeSingleEvent(of: .value, with: { snapshot in
			guard let playersDict = snapshot.value as? [String : Any] else { return }
			
			let players = Player.deserializeArray(from: playersDict)
			completion(players)
			
		}) { error in
			print(error.localizedDescription)
			completion(nil)
		}
	}
	
}

//MARK: UITableView data source
extension RoasterViewModel: UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return players.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCell", for: indexPath) as? PlayerTableViewCell {
			cell.player = players[indexPath.row].player
			
			if players[indexPath.row].isSelected {
				tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
			} else {
				tableView.deselectRow(at: indexPath, animated: true)
			}
			
			cell.accessoryType = cell.isSelected ? .checkmark : .none
			cell.selectionStyle = .none
			
			return cell
		}
		
		return UITableViewCell()
	}
	
}
