//
//  AccommodationViewModel.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 11/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase

class AccommodationViewModel {
	
	var registration: Registration
	
	
	init(_ registration: Registration) {
		self.registration = registration
	}
	
	func getOptionText(for option: Option) -> String {
		return "\(option.title) (\(option.price) Kč)"
	}
	
	func submitRegistration(completion: @escaping (Bool) -> ()) {
		guard let tournamentKey = registration.tournament.key else { return }
		guard let teamID = UIDevice.current.identifierForVendor?.uuidString else { return }
		let databaseReference = Database.database().reference()
		
		let value: [String: Any] = [
			"tournamentId": registration.tournament.key!,
			"teamId": teamID,
			"roaster": registration.roaster!,
			"dinners": registration.dinnerCounts,
			"accommodation": registration.accommodationCounts,
			"totalPrice": registration.getTotalPrice()
		]
		
		// Add to registrations
		let newRegistrationChild = databaseReference.child("registrations").childByAutoId()
		newRegistrationChild.setValue(value, andPriority: 1, withCompletionBlock: { error, ref in
			if let error = error {
				print("Error submiting registration: \(error.localizedDescription)")
				completion(false)
				return
			}
			completion(true)
		})
		
		// Add teamID to tournament registered teams
		let newRegisteredTeamChild = databaseReference.child("tournaments").child(tournamentKey).child("registeredTeams").childByAutoId()
		newRegisteredTeamChild.setValue(teamID)
	}
	
}
