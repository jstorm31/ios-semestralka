//
//  RoasterItemViewModel.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 10/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import Foundation

struct RoasterViewModelItem {
	var player: Player
	var isSelected = false
	
	init(player: Player) {
		self.player = player
	}
}
