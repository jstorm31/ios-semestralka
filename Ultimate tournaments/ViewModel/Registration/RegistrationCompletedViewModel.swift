//
//  CompletedViewModel.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 11/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import Foundation

class RegistrationCompletedViewModel {
	
	let registration: Registration
	
	var price: Int {
		return 450
	}
	
	init(_ registration: Registration) {
		self.registration = registration
	}
}
