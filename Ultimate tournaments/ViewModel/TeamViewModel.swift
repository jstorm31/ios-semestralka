//
//  TeamViewModel.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 09/02/2018.
//  Copyright © 2018 Jiří Zdvomka. All rights reserved.
//

import Foundation

class TeamViewModel {
	
	// Parses player's name and number from text
	static func parsePlayer(fromText text: String) -> Player? {
		// Extract name
		let name = text.components(separatedBy: " ").dropLast().joined(separator: " ")
		
		// Extract number
		let stringArray = text.split(separator: " ") // Split text
		let components = stringArray.last?.components(separatedBy: NSCharacterSet.decimalDigits.inverted)
		let extractedNumber = components?.joined()
		
		if extractedNumber == "" || name == "" {
			return nil
		}

		// Return Player object or nil
		guard let number = Int(extractedNumber!) else {
			return nil
		}
		return Player(name, number)
	}
	
}
