//
//  AppDelegate.swift
//  Ultimate tournaments
//
//  Created by Jiří Zdvomka on 28/12/2017.
//  Copyright © 2017 Jiří Zdvomka. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		window = UIWindow(frame: UIScreen.main.bounds)
		
		FirebaseApp.configure()
		
		let tournamentsController = UINavigationController(rootViewController: TournamentsViewController())
		tournamentsController.tabBarItem.title = "Turnaje"
		tournamentsController.tabBarItem.image = UIImage(named: "icon-ultimate")
		
		let finishedTournamentsController = UINavigationController(rootViewController: FinishedTournamentsViewController())
		finishedTournamentsController.tabBarItem.title = "Skončené"
		finishedTournamentsController.tabBarItem.image = UIImage(named: "icon-star")
		
		let teamController = UINavigationController(rootViewController: TeamViewController())
		teamController.tabBarItem.title = "Tým"
		teamController.tabBarItem.image = UIImage(named: "icon-trophy")
		
		let tabBarController = UITabBarController()
		tabBarController.viewControllers = [tournamentsController, finishedTournamentsController, teamController]
		UITabBar.appearance().tintColor = UIColor.MyTheme.blue
		UITabBar.appearance().backgroundColor = UIColor.MyTheme.grayBackground
		
		window?.rootViewController = tabBarController
		window?.makeKeyAndVisible()
		
		return true
	}

	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}

	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}

}

